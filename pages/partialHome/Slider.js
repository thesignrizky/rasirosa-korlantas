import React from 'react';
import { Col } from 'antd';
import { Text } from '@components';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y, Autoplay } from 'swiper';
// import 'swiper/swiper.scss';
// import 'swiper/components/pagination/pagination.scss';
// import 'swiper/components/navigation/navigation.scss';
import "../../styles/swiper.css";

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Autoplay]);

const Slider = ({slider}) => {
  var _slide_1 = {
    slidesPerView: 1,
    loop: true,
    autoplay: {
      delay: 5000,
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    },
    navigation: {
      nextEl: '.btn-next-1',
      prevEl: '.btn-prev-1'
    },
  };

  // var sliderImg = [
  //   {img: '/img/slide/rasirosa-slider-01.png', desc: "Public Safety Center"},
  //   {img: '/img/slide/rasirosa-slider-02.png', desc: "Safety Driving Center"},
  //   {img: '/img/slide/rasirosa-slider-03.png', desc: "Electronic Registration Identification"},
  //   {img: '/img/slide/rasirosa-slider-04.png', desc: "Safety Security Center"},
  //   {img: '/img/slide/rasirosa-slider-05.png', desc: "Traffic Management Center"},
  // ]

  return (
    <Col span={24} className="homeSlide">
      <Swiper {..._slide_1} >
        {slider && slider.map((res, i) => {
          return(
            <SwiperSlide key={i}>
              <Col span={24} className="homeSlideIns">
                <Col span={24} className="_img">
                  <img src={res} width="100%" alt="rasirosa-slider-01" />
                </Col>
                {/* <Col span={14} offset={2} className="_ttl">
                  <Text color="white" bold={true}>{res.desc}</Text>
                </Col> */}
              </Col>
            </SwiperSlide>
          )
        })}
      </Swiper>
      <Col className="swiper-button-prev btn-prev-1"></Col>
      <Col className="swiper-button-next btn-next-1"></Col>
      {/* <Col span={24} className="swiper-pagination"></Col> */}
    </Col>
  );
}

export default Slider;