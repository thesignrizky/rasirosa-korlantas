import React from 'react';
import { Row, Col, Avatar } from 'antd';
import Link from 'next/link'
import { Text } from '@components';
import { SecurityScanOutlined, ReadOutlined, CarOutlined, UserOutlined, SoundOutlined, UsergroupAddOutlined, BookOutlined, BankOutlined, RiseOutlined } from '@ant-design/icons';

const Category = () => {
  return (
    <Col span={20} offset={2} className="subMenu">
      <Row type="flex" justify="center" align="middle">
        <Col xs={8} lg={3} className="subMenuIns" align="center">
          <Link href="/category/[slug]" as="/category/keamanan">
            <a>
              <Avatar icon={<SecurityScanOutlined />} style={{ backgroundColor: '#00A2EA' }} size={{xs: 70, md: 80, lg: 97, xl: 97, xxl: 97}}/>
              <Col className="_txt" align="center"><Text size={15}>Keamanan</Text></Col>
            </a>
          </Link>
          <Col className="subCategory">
            <Text>test</Text>
          </Col>
        </Col>
        <Col xs={8} lg={3} className="subMenuIns">
          <Link href="/category/[slug]" as="/category/keselamatan">
            <a>
              <Avatar icon={<CarOutlined />} style={{ backgroundColor: '#00A2EA' }} size={{xs: 70, md: 80, lg: 97, xl: 97, xxl: 97}}/>
              <Col className="_txt" align="center"><Text size={15}>Keselamatan</Text></Col>
            </a>
          </Link>
        </Col>
        <Col xs={8} lg={3} className="subMenuIns">
          <Link href="/category/[slug]" as="/category/hukum">
            <a>
              <Avatar icon={<ReadOutlined />} style={{ backgroundColor: '#00A2EA' }} size={{xs: 70, md: 80, lg: 97, xl: 97, xxl: 97}}/>
              <Col className="_txt" align="center"><Text size={15}>Hukum</Text></Col>
            </a>
          </Link>
        </Col>
        <Col xs={8} lg={3} className="subMenuIns">
          <Link href="/category/[slug]" as="/category/administrasi">
            <a>
              <Avatar icon={<BookOutlined />} style={{ backgroundColor: '#00A2EA' }} size={{xs: 70, md: 80, lg: 97, xl: 97, xxl: 97}}/>
              <Col className="_txt" align="center"><Text size={15}>Administrasi</Text></Col>
            </a>
          </Link>
        </Col>
        <Col xs={8} lg={3} className="subMenuIns">
          <Link href="/category/[slug]" as="/category/informasi">
            <a>
              <Avatar icon={<SoundOutlined />} style={{ backgroundColor: '#00A2EA' }} size={{xs: 70, md: 80, lg: 97, xl: 97, xxl: 97}}/>
              <Col className="_txt" align="center"><Text size={15}>Informasi</Text></Col>
            </a>
          </Link>
        </Col>
        <Col xs={8} lg={3} className="subMenuIns">
          <Link href="/category/[slug]" as="/category/kemanusiaan">
            <a>
              <Avatar icon={<UsergroupAddOutlined />} style={{ backgroundColor: '#00A2EA' }} size={{xs: 70, md: 80, lg: 97, xl: 97, xxl: 97}}/>
              <Col className="_txt" align="center"><Text size={15}>Kemanusiaan</Text></Col>
            </a>
          </Link>
        </Col>
      </Row>

    </Col>
    
  );
}

export default Category;