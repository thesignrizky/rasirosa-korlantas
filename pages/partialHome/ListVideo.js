import React from 'react';
import { Row, Col } from 'antd';
import Link from 'next/link'
import { Text } from '@components';
import moment from 'moment'

const ListPost = ({ data }) => {
  return (
    <Col span={20} offset={2} className="homeNews mt-50">
      <Row gutter={[4, 4]}>
        <Col span={24} className="titleNews mb-20">
          <Text bold={true}>Video</Text>
        </Col>
        {data && data.map((res, i) => {
          return(
            <Col xs={24} lg={6} className="homeNewsIns" key={i}>
              <Col span={24} style={{height: 200}}>
                <span dangerouslySetInnerHTML={{__html: res.fimg_url.file}} style={{width: 100}}></span> 
              </Col>
            </Col>
          )
        })}
      </Row>
    </Col>
  );
}

export default ListPost;