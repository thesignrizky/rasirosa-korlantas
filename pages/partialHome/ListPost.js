import React from 'react';
import { Row, Col } from 'antd';
import Link from 'next/link'
import { Text } from '@components';
import moment from 'moment'

const ListPost = ({ post }) => {
  return (
    <Col span={20} offset={2} className="homeNews mt-50">
      <Row gutter={[4, 4]}>
        <Col span={24} className="titleNews mb-20">
          <Text bold={true}>Berita Terbaru</Text>
        </Col>
        {post && post.map((res, i) => {
          if(res.fimg_url.type === 'photo'){
            return(
              <Col xs={24} lg={6} className="homeNewsIns" key={i}>
                <Link href={{ pathname: '/post/[slug]', query: { name: 'test' } }} as={`/post/${res.slug}`}>
                  <a className="homeNewsInsLink">
                    <Col span={24} className="_img">
                    <img src={res.fimg_url.file ? res.fimg_url.file : '/img/img-default.jpg'} width="100%" alg={res.title.rendered}/>
                    </Col>
                    <Col span={24} className="_ttl">
                      <Col span={24}><Text color="soft-grey" size={12}>{moment(res.date).format('DD MMMM YYYY HH:mm:ss')}</Text></Col>
                      <Text color="white" bold={true}>
                        <span dangerouslySetInnerHTML={{__html: res.titleExcerpt}} />...
                      </Text>
                    </Col>
                  </a>
                </Link>
              </Col>
            )
          }else{
            return(
              <Col xs={24} lg={6} className="homeNewsIns" key={i}>
                <Col span={24} style={{height: '100%'}}>
                  <span dangerouslySetInnerHTML={{__html: res.fimg_url.file}} style={{width: 100}}></span> 
                </Col>
              </Col>
            )
          }
        })}
      </Row>
    </Col>
  );
}

export default ListPost;