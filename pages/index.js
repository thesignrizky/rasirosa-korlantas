import React from 'react';
import { Row } from 'antd';
import { useRouter } from 'next/router';
import { setHomePost } from '@actions/postAction';
import { Header, Loading } from '@components';
import Category from './partialHome/Category';
import Slider from './partialHome/Slider';
import ListPost  from './partialHome/ListPost';
import ListVideo  from './partialHome/ListVideo';

const Home = ({ post, video, slider }) => {
  const router = useRouter();
  if(router.isFallback){
    return <Loading />
  }
  return (
    <Header title="Home">
      <Row type="flex" justify="start" className="homePage">
        <Slider slider={slider} />
        <Category />
        <ListPost post={post} />
        <ListVideo data={video} />
      </Row>
    </Header>
  );
}

export async function getServerSideProps() {
  var payload = {
    per_page: 4,
  }
  const data = await setHomePost(payload);  
  return {
    props: {
      post: data.post,
      video: data.video,
      slider: data.slider,
    },
  }
}
export default Home;