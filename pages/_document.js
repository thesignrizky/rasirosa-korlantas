import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon"/> 
          <meta name="viewport" content="viewport-fit=cover" />
          <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
          <meta httpEquiv="Content-Type" content="text/html; charset=ISO-8859-1" />

          <meta name="description" content="Dari judul di atas timbul pertanyaan mengapa pemolisian di bidang lalu lintas perlu memikirkan road safety ( lalu lintas yg aman selamat tertib dan lancar)" />
          
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:site" content="@rasirosa" />
          <meta name="twitter:title" content="Rasirosa Korlantas" />
          <meta name="twitter:description" content="Dari judul di atas timbul pertanyaan mengapa pemolisian di bidang lalu lintas perlu memikirkan road safety ( lalu lintas yg aman selamat tertib dan lancar)" />
          <meta name="twitter:creator" content="@rasirosa" />
          <meta name="twitter:image" content="/img/thumbnail.png" />

          <meta property="og:type" content="website" />
          <meta property="og:url" content="http://rasirosakorlantas.id/" />
          <meta property="og:title" content="Rasirosa Korlantas" />
          <meta property="og:description" content="Dari judul di atas timbul pertanyaan mengapa pemolisian di bidang lalu lintas perlu memikirkan road safety ( lalu lintas yg aman selamat tertib dan lancar)" />
          <meta property="og:image" content="/img/thumbnail.png" />

        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
