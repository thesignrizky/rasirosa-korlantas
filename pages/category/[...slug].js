import React from 'react';
import { useRouter } from 'next/router'
import { Row, Col, Typography, Empty } from 'antd';
import { setListPost } from '@actions/postAction';
import { Header, LoadingCard } from '@components';
import ListCategory from './ListCategory';
import ListNews from './ListNews';
import Sidebar from '../post/Sidebar'
const { Title } = Typography;

const Category = ({ post }) => {
  const router = useRouter();
  var slug = router.query.slug ? router.query.slug[0] : [];
  console.log(999, post);
  return(
    <Header title={slug}>
      <Row type="flex" justify="start" className="listCategory">
        <Col span={24} className="banner">
          <Col span={24} className="_img">
            <img src="/img/banner.jpg" alt="banner" width="100%" />
          </Col>
          <Col span={24} className="_ttl">
            <Title level={1}>{slug.replace(/-+/g, ' ')}</Title>
          </Col>
        </Col>
        <Col span={20} offset={2} className="mt-50">
          <Row>
            <Col xs={24} lg={14}> 
              {
                router.isFallback ? <LoadingCard row={3} total={12} /> 
                : post && post.length > 0 ? 
                  <ListNews post={post} router={router} /> 
                : <Empty />
              }
            </Col>
            <Col lg={{span: 24, offset: 0}} lg={{span: 8, offset: 2}} >
              <Sidebar related={post.length > 0 ? post[0].related : []} />
            </Col>
          </Row> 
        </Col>
      </Row>
    </Header> 
  )
}

export async function getServerSideProps({ params }) {
  const slug = params.slug[0];
  const page = params.slug[1];
  var payload = {
    "per_page": 12,
    "page": page,
    "filter": {
      "category_name": slug
    }
  }
  const data = await setListPost(payload);
  return {
    props: {
      post: data,
    }
  }
}

export default Category;