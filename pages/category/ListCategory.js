import React from 'react';
import { Row, Col } from 'antd';
import { Text } from '@components';
import { LaptopOutlined, EditOutlined, ClockCircleOutlined, SettingOutlined, BookOutlined, RiseOutlined } from '@ant-design/icons';
import Link from 'next/link';

const ListCategory = ({slug}) => {
  return(
    <Col>
      <Link href="/category/[...slug]" as="/category/kamsel" scroll={false}>
        <a>
          <Row align="middle" className={`listMenuIns ${slug === "kamsel" && "active" }`}>
            <Col span={4} className="_ic">
              <LaptopOutlined />
            </Col>
            <Col span={18} offset={2}>
              <Text size={16} bold={true}>Kamsel</Text>
            </Col>
          </Row>
        </a>
      </Link>
      <Link href="/category/[...slug]" as="/category/gakkum" scroll={false}>
        <a>
          <Row align="middle"  className={`listMenuIns ${slug === "gakkum" && "active" }`}>
            <Col span={4} className="_ic">
              <LaptopOutlined />
            </Col>
            <Col span={18} offset={2}>
              <Text size={16} bold={true}>Gakkum</Text>
            </Col>
          </Row>
        </a>
      </Link>
      <Link href="/category/[...slug]" as="/category/registrasi" scroll={false}>
        <a>
          <Row align="middle"  className={`listMenuIns ${slug === "registrasi" && "active" }`}>
            <Col span={4} className="_ic">
              <EditOutlined />
            </Col>
            <Col span={18} offset={2}>
              <Text size={16} bold={true}>Registrasi</Text>
            </Col>
          </Row>
        </a>
      </Link>
      <Link href="/category/[...slug]" as="/category/operasional" scroll={false}>
        <a>
          <Row align="middle"  className={`listMenuIns ${slug === "operasional" && "active" }`}>
            <Col span={4} className="_ic">
              <ClockCircleOutlined />
            </Col>
            <Col span={18} offset={2}>
              <Text size={16} bold={true}>Operasional</Text>
            </Col>
          </Row>
        </a>
      </Link>
      <Link href="/category/[...slug]" as="/category/teknologi" scroll={false}>
        <a>
          <Row align="middle"  className={`listMenuIns ${slug === "teknologi" && "active" }`}>
            <Col span={4} className="_ic">
              <SettingOutlined />
            </Col>
            <Col span={18} offset={2}>
              <Text size={16} bold={true}>Teknologi</Text>
            </Col>
          </Row>
        </a>
      </Link>
      <Link href="/category/[...slug]" as="/category/pustaka" scroll={false}>
        <a>
          <Row align="middle"  className={`listMenuIns ${slug === "pustaka" && "active" }`}>
            <Col span={4} className="_ic">
              <BookOutlined />
            </Col>
            <Col span={18} offset={2}>
              <Text size={16} bold={true}>Pustaka</Text>
            </Col>
          </Row>
        </a>
      </Link>
      <Link href="/category/[...slug]" as="/category/inovasi" scroll={false}>
        <a>
          <Row align="middle"  className={`listMenuIns ${slug === "inovasi" && "active" }`}>
            <Col span={4} className="_ic">
              <RiseOutlined />
            </Col>
            <Col span={18} offset={2}>
              <Text size={16} bold={true}>Inovasi</Text>
            </Col>
          </Row>
        </a>
      </Link>
    </Col>
  )
}

export default ListCategory;