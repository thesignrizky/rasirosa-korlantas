import React from 'react';
import Link from 'next/link'
import { Row, Col, Pagination } from 'antd';
import { Text } from '@components';
import moment from 'moment';

export default function ListNews({ post, router }){
  const slug = router.query.slug ? router.query.slug[0] : [];
  const page = router.query.slug ? router.query.slug[1] || 1 : 1;
  return(
    <Row span={24} gutter={[4, 4]} className="listCategoryNew">
      <Col span={24} className="mb-10"><Text size={25} bold={true}>Berita</Text></Col>
      {post && post.map((res, i) => {
        if(res.fimg_url.type === 'photo'){
          return(
            <Col xs={24} lg={8} key={i} className="listNews">
              <Link href={{ pathname: '/post/[slug]', query: { name: 'test' } }} as={`/post/${res.slug}`}>
                <a>
                  <Col span={24} className="_img">
                    <img src={res.fimg_url ? res.fimg_url.file : '/img/img-default.jpg'} width="100%" alg={res.title.rendered}/>
                  </Col>
                  <Col span={24} className="_ttl">
                    <Col span={24}><Text color="soft-grey" size={12}>{moment(res.date).format('DD MMMM YYYY HH:mm:ss')}</Text></Col>
                    <Text color="white" bold={true}>
                      <span dangerouslySetInnerHTML={{__html: res.titleExcerpt}} />
                    </Text>
                  </Col>
                  </a>
              </Link>
            </Col>
          )
        }else{
          return(
            <Col xs={24} lg={8} className="listNews" key={i}>
              <Col span={24} style={{height: '100%'}}>
                <Col dangerouslySetInnerHTML={{__html: res.fimg_url.file}}></Col> 
              </Col>
            </Col>
          )
        }
      })}
      <Col span={24} align="center" className="mt-30">
        <Pagination 
          pageSize={12}
          current={parseFloat(page)} 
          total={post && post.length > 0 ? post[0].totalPost : 0} 
          onChange={(page) => 
            router.push(
              `/category/[...slug]`, 
              `/category/${slug}/${page}`
            )
          }
        />
      </Col>
    </Row>
  )
}
