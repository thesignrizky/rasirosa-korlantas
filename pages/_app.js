import App from 'next/app';
import React from 'react';
import { withRouter, Router } from 'next/router'
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import "../styles/antd.less";
import "../styles/globals.css";
import "../styles/styles.css";
import "../styles/wordpress.scss";

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());
class MyApp extends App {  
  render() {
    const { Component, pageProps, router } = this.props;
    return <Component router={router} {...pageProps} /> 
  }
}

export default withRouter(MyApp)