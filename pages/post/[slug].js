import { Row, Col, Typography, Divider }from 'antd';
import { useRouter } from 'next/router'
import { setDetailPost } from '@actions/postAction';
import { Header, Loading, Text } from '@components';
import Sidebar from './Sidebar'
import moment from 'moment';
const { Title } = Typography;

const Post = ({post}) => {
  const router = useRouter()
  console.log(222, post);
  if(router.isFallback){
    return <Loading />
  }
  return(
    <Header title="Category">
      <Row className="detailPage mt-50 mtm-30">
        <Col span={20} offset={2}>
          <Row className="mb-20">
            <Col xs={24} lg={14}>
              <Title level={1}><span dangerouslySetInnerHTML={{__html: post.title.rendered}} /></Title>
              <Col span={24}>
                <Text color="soft-grey" size={14}>{moment(post.date).format('DD MMMM YYYY HH:mm:ss')}</Text>
                <Divider type="vertical" />
                <Text color="soft-grey" size={14}>By {post.authorName}</Text>
              </Col>
            </Col>
          </Row>

          <Row className="pageIns">
            <Col xs={24} lg={14}> 
              <Col span={24}>
                <Text size={18}><span className="qq" dangerouslySetInnerHTML={{__html: post.content.rendered}} /></Text>
              </Col>
            </Col>
            <Col lg={{span: 24, offset: 0}} lg={{span: 8, offset: 2}} className="mtm-40">
              <Sidebar related={post.related} />
            </Col>
          </Row>
        </Col>
      
      </Row>
    </Header>
  )
}

export async function getServerSideProps({ params, preview = false, previewData }) {
  const data = await setDetailPost(params.slug);
  return {
    props: {
      preview,
      post: data,
    },
  }
}

export default Post;