import React from 'react';
import { Col, Divider } from 'antd';
import { Text } from '@components';
import Link from 'next/link'
import moment from 'moment';

const Sidebar = ({ related }) => {
  return (
    <Col span={24}>
      <Col span={24} className="mb-20"><Text size={20} bold={true}>Berita Lainnya</Text></Col>
      {related ? related.map((res, i) => {
        return(
          <Col span={24} key={i}>
            <Link href={{ pathname: '/post/[slug]', query: { name: 'test' } }} as={`/post/${res.post_name}`}>
              <a>
                <Col span={24} className="mb-5"><Text size={14} color="soft-grey">{moment(res.post_date).format('DD MMMM YYYY HH:mm:ss')}</Text></Col>
                <Col span={24}><Text size={16} bold={true}><span dangerouslySetInnerHTML={{__html: res.post_title}} /></Text></Col>
                <Col span={24} className="mb-5"><Text size={14} color="soft-grey">{res.post_category}</Text></Col>
              </a>
            </Link>
            <Divider />
          </Col>
        )
      }) : null}
    </Col>
  );
}

export default Sidebar;