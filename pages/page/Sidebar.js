import React from 'react';
import { Row, Col, Typography, Divider }from 'antd';
import { Loading, Text } from '@components';
const { Title } = Typography;

const Sidebar = ({ post }) => {
  return(
    <Col span={24} className="p-20 bg-soft-grey mt-30">
      {post && post.map((res, i) => {
        return(
          <Col span={24} key={i}>
            <Col span={24} className="_img">
              <img src={res.fimg_url ? res.fimg_url : '/img/img-default.jpg'} width="100%" alg={res.title.rendered}/>
            </Col>
            <Col span={24}>
              <Text size={16} bold={true}>{res.title.rendered}</Text>
            </Col>
            <Divider />
          </Col>
        )
      })}
    </Col>
  )
}
export default Sidebar;