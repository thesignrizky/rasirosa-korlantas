import React from 'react';
import { Row, Col, Typography, Empty }from 'antd';
import { useRouter } from 'next/router'
import { setDetailPage } from '@actions/pageAction';
import { Header, Loading, Text } from '@components';
import moment from 'moment';
// import Sidebar from './Sidebar'
const { Title } = Typography;

const Page = ({ page }) => {
  const router = useRouter();
  return(
    <Header title="Page">
      <Row className="detailPage mt-50 mtm-30">
        <Col span={20} offset={2}>
          <Row>
          {
            router.isFallback ? <Loading />
            : page ?
              <Col span={24}>
                <Col span={24} className="mb-40">
                  <Title level={1}><span dangerouslySetInnerHTML={{__html: page.title.rendered}} /></Title>
                  <Col span={24}>
                    <Text color="soft-grey" size={14}>{moment(page.date).format('DD MMMM YYYY HH:mm:ss')}</Text>
                  </Col>
                </Col>

                <Col span={24} className="pageIns">
                  <Text size={18}><span dangerouslySetInnerHTML={{__html: page.content.rendered}} /></Text>
                </Col>
              </Col>
            : <Col span={24}><Empty /></Col>
          }

          </Row>
        </Col>
      </Row>
    </Header>
  )
}

export async function getServerSideProps({ params }) {
  const page = await setDetailPage(params.slug);
  return {
    props: {
      page: page ? page : null,
    },
  }
}

export default Page;