import React from 'react';
import { Result, Button } from 'antd';
import { Header } from '@components';
import Link from 'next/link';

const Custom404 = () => {
  return (
    <Header title="404 Not Found">
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={
          <Link href="/">
            <a>
            <Button type="primary">Back Home</Button>
            </a>
          </Link>
        }
      />
    </Header>
  )
}
export default Custom404