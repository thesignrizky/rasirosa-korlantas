import Ws from '@adonisjs/websocket-client';
import moment from 'moment'

class WebsocketSource {
  constructor() {
    this.connection = null;
  };
  
  connect(id) {
    this.connection = new Ws('wss://ace-api.ace.team/api/', { path: 'v1' }).connect();
    this.connection.channel = this.connection.subscribe(`message:${id}`);
    return this;
  }

  send(data, cb){
    this.connection.channel.emit('message', data);
    this.connection.channel.on('message', (res) => {
      return cb(res)
    })
  }

  onmessage(cb){
    this.connection.channel.on('message', (res) => {
      return cb(res)
    })

  }
}

export default new WebsocketSource();
