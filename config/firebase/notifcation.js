import { messaging } from './firebase';

export const requestPermission = async ()  => {
  if (!('Notification' in window)) {
    console.log("This browser does not support notifications.");
  } else {
    if(await checkNotificationPromise()) {
      Notification.requestPermission()
      .then((permission) => {
        return handlePermission(permission);
      })
    } else {
      Notification.requestPermission(function(permission) {
        return handlePermission(permission);
      });
    }
  }
}

export const checkNotificationPromise = async () => {
  try {
    Notification.requestPermission().then();
  } catch(e) {
    return false;
  }
  return true;
}

export const handlePermission = async (permission) => {
  if(!('permission' in Notification)) {
    Notification.permission = permission;
  }
  if(Notification.permission === 'denied' || Notification.permission === 'default') {
    // console.log(222, 'no notif');
  } else {
    // console.log(222, 'ok notif');
    // return notificationPermission();
  }
}

export const notificationPermission = async ()  => {
  // let permissionGranted = false;
  let newToken = null
  try {
      if (Notification.permission !== 'granted') {
          await messaging.requestPermission();
      }else{
        const token = await messaging.getToken();
        // permissionGranted = true;
        newToken = token;
      }
      // if (localStorage.getItem('INSTANCE_TOKEN') !== null) {
      //     permissionGranted = true;
      //     console.log(222, 'INSTANCE_TOKEN');
          
      // } else {
      //     const token = await messaging.getToken();
      //     console.log(7776, token);
      //     localStorage.setItem('INSTANCE_TOKEN', token);
      //     permissionGranted = true;
      // }
  } catch (err) {
      console.log(err);
      if (err.hasOwnProperty('code') && err.code === 'messaging/permission-default') console.log('You need to allow the site to send notifications');
      else if (err.hasOwnProperty('code') && err.code === 'messaging/permission-blocked') console.log('Currently, the site is blocked from sending notifications. Please unblock the same in your browser settings');
      else console.log('Unable to subscribe you to notifications');
  } finally {
      // return permissionGranted;
      return newToken;
  }
}