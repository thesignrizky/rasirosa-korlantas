import firebase from 'firebase/app';
import '@firebase/messaging';

const config = {
  apiKey: "AIzaSyD3_GCQpY--wmZ2cro0xHSRByq2E0IM9iI",
  authDomain: "ace-team-push.firebaseapp.com",
  databaseURL: "https://ace-team-push.firebaseio.com",
  projectId: "ace-team-push",
  storageBucket: "ace-team-push.appspot.com",
  messagingSenderId: "1021330560810",
  appId: "1:1021330560810:web:b9f45930dd0aaad290b053",
  measurementId: "G-V0ZKHLCR6P"
};

const initializedFirebaseApp = firebase.initializeApp(config);
var messaging;

if(firebase.messaging.isSupported()) {
  messaging = initializedFirebaseApp.messaging()
  messaging.usePublicVapidKey('BAzFxE6smq6rDdqwlRmwL4bHb3z_JJmoBImVzPmKncugXAt_YAO_H85gAmuRhRX97b8nD6hNqiBzV1awxB8HGXU');
}

export { messaging };