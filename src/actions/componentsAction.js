
import { API} from '../../config';
import { errorHandler } from './errorAction';

export const setCategoryMenu = (successCB, failedCB) => async dispatch => {
  await dispatch({type: 'SET_CATEGORY_MENU'});
  return API.GET('/categories', (res) => {
    dispatch({ type: 'SET_CATEGORY_MENU_SUCCESS', payload: {
      "data": res.data
    }});
    return successCB && successCB()
  }, (err) => {
    return dispatch(errorHandler(
      err, 
      failedCB && failedCB(),
      dispatch({ type: 'SET_CATEGORY_MENU_FAILED' })
    ));
  })
}
