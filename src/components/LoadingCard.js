import React from 'react';
import { Row, Col, Skeleton } from 'antd';

const LoadingCard = ({total, row}) =>{
  var span = 24 / row;
  const rowArray = new Array(total).fill('total');
  return (
    <Row gutter={[24, 24]}>
      {rowArray.map((res, i) => {
        return( 
          <Col span={parseFloat(span)} key={i}>
            <Skeleton active />
          </Col> 
          )
      })}
    </Row>
  );
}

export default LoadingCard;