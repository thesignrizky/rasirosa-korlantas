import React from 'react';
import { Spin, Row } from 'antd';
import { Header } from './index';

const Loading =() => {
  return (
    <Header title="Loading">
      <Row type="flex" justify="space-around" align="middle" style={{alignItems: 'center', height: '100vh'}}>
        <Spin tip="Loading..." size="large" className="color-blue"/> 
      </Row>
    </Header>
  );
}

export default Loading;