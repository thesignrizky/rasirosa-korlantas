import React, { Component } from 'react';
import { Typography } from 'antd';
const { Text, Paragraph } = Typography;

export default class TextComponent extends Component {
  colorCondition(c){
    switch (c) {
      case 'gold':
        return '#B49B57'
      case 'soft-grey':
        return '#b3b3b3'
      case 'grey':
        return '#4E4E4E'
      case 'black':
        return '#3E3E3E'
      case 'red':
        return '#FD1713'
      case 'soft-blue':
        return '#00A2EA'
      case 'blue':
        return '#0D60A9'
      case 'white':
        return '#fff'
      default: 
        return '#3E3E3E'
    }
  }
  render() {
    const {color, bold, size, children, ellipsis, paragraph} = this.props;
    if(!paragraph){
      return (
        <Text className={`text-size-${size}`} ellipsis={ellipsis ? true : false} style={{color: this.colorCondition(color), fontWeight: bold ? 900 : 400, fontSize: size, width: ellipsis ? '100%' : 'inherit'}}>
          {children}
        </Text>
      );
    }
    return (
      <Paragraph ellipsis={{rows: ellipsis}} style={{color: this.colorCondition(color), fontWeight: bold ? 'bold' : 500, fontSize: size, width: '100%'}}>
        {children}
      </Paragraph>
    );
  }
}
