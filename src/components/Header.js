import React from 'react';
import { Row, Col, Layout } from 'antd';
import { useRouter } from 'next/router';
import Head from 'next/head'
import Link from 'next/link';
import { Menu, Text } from './index';
import { FacebookOutlined, TwitterOutlined, YoutubeOutlined, InstagramOutlined } from '@ant-design/icons';
const { Header, Content, Footer } = Layout;

const Headers = (props) => {
  const router = useRouter()
  return (
    <Layout className="layout">
      <Head>
        <title>Rasirosa - {props.title}</title>
      </Head>

      <Header className={`${props.type}`}>
        <Col span={20} offset={2}>
          <Row align="middle">
            <Col xs={4} lg={2} className="logo">
              <Link href="/">
                <a>
                  <img src="/img/logo.png" width="100%" alt="logo" />
                </a>
              </Link>
            </Col>

            <Col xs={{span: 3, offset: 17}} lg={{span: 22, offset: 0}} align="end">
              <Menu currentPage={router.query.slug} /> 
            </Col>
          </Row>
        </Col>
      </Header>
      
      <Content style={{ minHeight: '100vh' }}>
        <div className="site-layout-content">
          {props.children}
        </div>
      </Content>

      <Footer className="footer mt-100">
        <Col span={20} offset={2}>
          <Row align="middle">
            <Col xs={24} lg={12}>
              <Text color="white" size={16}>©2020 Korlantas. All rights reserved.</Text>
            </Col>
            <Col xs={24} lg={12} align="end" className="sosmed">
              <FacebookOutlined />
              <TwitterOutlined className="ml-20" />
              <YoutubeOutlined className="ml-20" />
              <InstagramOutlined className="ml-20" />
            </Col>
          </Row>
        </Col>
      </Footer>
    </Layout>
  );
}

export default Headers;