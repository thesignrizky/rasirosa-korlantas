import React  from 'react';
import { Menu } from 'antd';
import Link from 'next/link'
import Permission from '../data/Permission.json'
import { MenuOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router'
const { SubMenu } = Menu;

const Menus = ({ currentPage }) => {
  const router = useRouter();
  return (
    <Menu 
      mode="horizontal" 
      defaultSelectedKeys={[currentPage]} 
      inlineIndent={2} 
      overflowedIndicator={<MenuOutlined  />}
    >
      <Menu.Item key="about_">
        <Link href="/page/[slug]" as="/page/tentang-kami"><a>Tentang Kami</a></Link>
      </Menu.Item>
      {Permission.map((res, i ) => {
        if(res.subMenu){
          return(
            <SubMenu title={res.title} key={i} onTitleClick={(a, b) => router.push(
              `/category/[slug]`, 
              `/category/${res.slug}`
            )}>
              {res.subMenu.map((_res, _i) => {
                if(_res.group){
                  return(
                    <Menu.ItemGroup title={_res.group.title} key={_res.group}>
                      {_res.group.list.map((__res, __i) => {
                        return(
                        <Menu.Item key={__res.slug}>
                          <Link href="/category/[slug]" as={`/category/${__res.slug}`}><a>{__res.title}</a></Link>
                        </Menu.Item>
                        )
                      })}
                    </Menu.ItemGroup>
                  )
                }else{
                  return(
                    <Menu.Item key={_res.slug}>
                      <Link href="/category/[slug]" as={`/category/${_res.slug}`}><a>{_res.title}</a></Link>
                    </Menu.Item>
                  )
                }
              })}
            </SubMenu>
          )
        }else{
          return(
            <Menu.Item key={res.slug}>
              <Link href="/category/[slug]" as={`/category/${res.slug}`}><a>{res.title}</a></Link>
            </Menu.Item>
          )
        }
      })}
      <Menu.Item key="monev">
        <Link href="http://monev.rasirosakorlantas.id/"><a target="_blank">Monev</a></Link>
      </Menu.Item>
    </Menu>
  );
}
export default Menus;